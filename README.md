# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Azure References ###

[Labelling tool](https://docs.microsoft.com/en-us/azure/cognitive-services/form-recognizer/quickstarts/label-tool?tabs=v2-1)
[Client library for Form Recognizer](https://docs.microsoft.com/en-us/azure/cognitive-services/form-recognizer/quickstarts/client-library?tabs=preview%2Cv2-1&pivots=programming-language-python)
[Azure Custom Vision Service](https://azure.microsoft.com/en-in/services/cognitive-services/custom-vision-service/)
[Azure Computer Vision](https://azure.microsoft.com/en-in/services/cognitive-services/computer-vision/)
[Azure Form Recognizer](https://azure.microsoft.com/en-in/services/cognitive-services/form-recognizer/)
[Azure Cognitive Sevices Python Samples](https://github.com/Azure-Samples/cognitive-services-python-sdk-samples/tree/master/samples/vision)
[Index Image Analysis Azure](https://docs.microsoft.com/en-us/azure/cognitive-services/computer-vision/index-image-analysis)
[Text analytics user scenarios- Azure](https://docs.microsoft.com/en-us/azure/cognitive-services/text-analytics/text-analytics-user-scenarios)
[AI Builder Text recogniser](https://docs.microsoft.com/en-us/ai-builder/prebuilt-text-recognition)
[AI Builder Text recogniser](https://docs.microsoft.com/en-us/ai-builder/prebuilt-text-recognizer-component-in-powerapps)
[REST API for Form Recogniser](https://docs.microsoft.com/en-us/azure/cognitive-services/form-recognizer/quickstarts/client-library?pivots=programming-language-rest-api&tabs=preview%2Cv2-1)
[Overview OCR Azure](https://docs.microsoft.com/en-us/azure/cognitive-services/computer-vision/overview-ocr)
[Azure Form Recognizer Layout](https://docs.microsoft.com/en-us/azure/cognitive-services/form-recognizer/concept-layout)


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact